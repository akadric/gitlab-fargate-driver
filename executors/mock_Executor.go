// Code generated by mockery v2.18.0. DO NOT EDIT.

package executors

import (
	context "context"

	mock "github.com/stretchr/testify/mock"
)

// MockExecutor is an autogenerated mock type for the Executor type
type MockExecutor struct {
	mock.Mock
}

// Execute provides a mock function with given fields: ctx, connection, script
func (_m *MockExecutor) Execute(ctx context.Context, connection ConnectionSettings, script []byte) error {
	ret := _m.Called(ctx, connection, script)

	var r0 error
	if rf, ok := ret.Get(0).(func(context.Context, ConnectionSettings, []byte) error); ok {
		r0 = rf(ctx, connection, script)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

type mockConstructorTestingTNewMockExecutor interface {
	mock.TestingT
	Cleanup(func())
}

// NewMockExecutor creates a new instance of MockExecutor. It also registers a testing interface on the mock and a cleanup function to assert the mocks expectations.
func NewMockExecutor(t mockConstructorTestingTNewMockExecutor) *MockExecutor {
	mock := &MockExecutor{}
	mock.Mock.Test(t)

	t.Cleanup(func() { mock.AssertExpectations(t) })

	return mock
}
